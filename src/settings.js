module.exports = {

  title: 'Admin start point',

  /**
   * 是否固定顶部工具栏
   * @type {boolean} true | false
   * @description Whether fix the header
   */
  fixedHeader: true,

  /**
   * 是否显示菜单顶部logo
   * @type {boolean} true | false
   * @description Whether show the logo in sidebar
   */
  sidebarLogo: true
}
