import request from '@/utils/request'

export function login(data) {
  return request({
    url: '/admin-start-point/user/login',
    method: 'post',
    data
  })
}

export function getInfo(token) {
  return request({
    url: '/admin-start-point/user/info',
    method: 'get',
    params: { token }
  })
}

export function logout() {
  return request({
    url: '/admin-start-point/user/logout',
    method: 'post'
  })
}
