import request from '@/utils/request'

export function getList(params) {
  return request({
    url: '/admin-start-point/table/list',
    method: 'get',
    params
  })
}
