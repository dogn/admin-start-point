import defaultSettings from '@/settings'

const title = defaultSettings.title || 'Admin start point'

export default function getPageTitle(pageTitle) {
  if (pageTitle) {
    return `${pageTitle} - ${title}`
  }
  return `${title}`
}
