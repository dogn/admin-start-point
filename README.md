# admin-start-point

> 管理的起点，这是一个极简的 vue admin 管理后台。它只包含了 Element UI & axios & iconfont & permission control & lint，这些搭建后台必要的东西。

目前版本为 `v4.0+` 基于 `vue-cli` 进行构建

node v14.21.3

npm 6.14.18

## Build Setup

```bash
# 克隆项目
git clone https://gitee.com/dogn/admin-start-point.git

# 进入项目目录
cd admin-start-point

# 安装依赖
npm install

# 建议不要直接使用 cnpm 安装以来，会有各种诡异的 bug。可以通过如下操作解决 npm 下载速度慢的问题
npm install --registry=https://registry.npm.taobao.org

# 启动服务
npm run dev
```

浏览器访问 [http://localhost:7777](http://localhost:7777)

## 发布

```bash
# 构建测试环境
npm run build:stage

# 构建生产环境
npm run build:prod
```
Copyright (c) 2024-present DongQihong
